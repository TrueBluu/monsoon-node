## What is monsoon?

Monsoon is a decentralized file storage and retrieval protocol. It aims to fix the problems of similar applications like [Filecoin](https://filecoin.io/) and to provide a convenient interface for users.

## Documentation
* [Protocol Documentation]()
* [API]()

## Contributing
Here is a list of things that need to be done right now:

* Porting - Right now, the monsoon-node code base is written for Linux, to make it more accessible, contributors could port it to Windows or Mac.
* Features - Implementing new features.
* Security - Finding security vulnerabilities, reporting them and/or fixing them
* Janitor - Cleaning up and commenting the codebase, possibly the most important job

#include <Util/Util.hpp>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <errno.h>
#include "TcpConnection.hpp"

TcpConnection::TcpConnection(std::string _ip, int _port)
{
  failure = false;
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (fd == 0)
  {
    LOG_ERROR("Creating socket failed\n");
    failure = true;
    return;
  }

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(_port);

  if(inet_pton(AF_INET, "127.0.0.1", &address.sin_addr)<=0)
  {
    LOG_ERROR("inet_pton() failed\n");
    failure = true;
    return;
  }

  if (connect(fd, (struct sockaddr*)&address, sizeof(address)) < 0)
  {
    LOG_ERROR("connect() failed\n");

    failure = true;
    return;
  }

  const int       optVal = 1;
  const socklen_t optLen = sizeof(optVal);
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen) != 0)
  {
    LOG_INFO("setsockopt() failed, sockets may appear binded for a while after closing the program\n");
  }
}

int TcpConnection::s_send(uint8_t* data, uint64_t size)
{
  int result = send(fd, data,size,0);

  if (result == -1)
  {
    LOG_INFO("Sent 0 bytes of data\n");
    //0 bytes were sent
    return 0;
  }

  LOG_INFO("Sent " + std::to_string(result) + " bytes of data\n");
  //return how many were sent
  return result;
}

std::vector<unsigned char> TcpConnection::s_read(int size)
{
  std::vector<unsigned char> data;
  data.reserve(size);

  int count = recv(fd, data.data(),size,0);

  if (count == -1)
    return {};

  data.resize(count);

  return data;
}

void TcpConnection::wait_for_sig(PROTOCOL_ERR sig)
{
  uint16_t data;
  uint16_t check = htons(sig);
  while (data != check)
  {
    recv(fd, (unsigned char*)&data, 2, 0);
  }
}

void TcpConnection::send_sig(PROTOCOL_ERR sig)
{
  uint16_t byte = htons(sig);
  send(fd,&byte,2,0);
}

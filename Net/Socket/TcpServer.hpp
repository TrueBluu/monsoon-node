#pragma once
#include <cstdint>
#include <vector>
#include <string>
#include <Net/GeneralProtocol.hpp>
#include <sys/ioctl.h>

class TcpServer
{
public:
  TcpServer(int port);

  int s_accept();
  int s_send(uint8_t* data, uint64_t size);
  void send_sig(PROTOCOL_ERR sig);

  std::vector<unsigned char> s_read(int size);

  void s_close();

  int m_fd();
  int m_cfd();
  bool m_connected();
  size_t available();
  bool m_failure();
private:
  bool failure = false;
  bool connected = false;
  int fd;
  //client socket file descriptor
  int c_fd;

};

#pragma once
#include <cstdint>
#include <vector>
#include <string>
#include <unistd.h>
#include <Net/GeneralProtocol.hpp>
#include <sys/ioctl.h>

class TcpConnection
{
public:
  TcpConnection(std::string _ip, int _port);

  inline void s_close()
  {
    close(fd);
  }

  //returns amount of bytes readable in socket
  inline int available()
  {
    int count;
    ioctl(fd, FIONREAD, &count);

    return count;
  }

  inline bool m_failure()
  {
    return failure;
  }

  //Wait to receive a signal from the connected socket
  void wait_for_sig(PROTOCOL_ERR sig);
  //send signal to socket
  void send_sig(PROTOCOL_ERR sig);

  //returns how many bytes were sent
  int s_send(uint8_t* data, uint64_t size);
  std::vector<unsigned char> s_read(int size);

private:
  std::string ip;

  int port;
  int fd;

  bool failure = false;
};

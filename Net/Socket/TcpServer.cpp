#include "TcpServer.hpp"
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <iostream>
#include <Util/Util.hpp>

TcpServer::TcpServer(int port)
{
  failure = false;
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (fd == 0)
  {
    LOG_ERROR("socket() failed\n");
    failure = true;
    return;
  }

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(port);
  address.sin_addr.s_addr = INADDR_ANY;

  if (bind(fd, (struct sockaddr*)&address, sizeof(address)) < 0)
  {
    LOG_ERROR("bind() failed\n");
    failure = true;
    return;
  }

  if (listen(fd,3) == -1)
  {
    LOG_ERROR("listen() failed\n");
    failure = true;
    return;
  }

  connected = true;
}

std::vector<unsigned char> TcpServer::s_read(int size)
{
  std::vector<unsigned char> data;
  data.reserve(size);

  int read = recv(c_fd, data.data(),size,0);

  if (read == -1)
  {
      std::cout << strerror(errno);
      return {};
  }

  data.resize(read);

  return data;
}

int TcpServer::s_send(uint8_t* data, uint64_t size)
{
  int result = send(c_fd, data,size,0);

  if (result == -1)
  {
    LOG_INFO("Sent 0 bytes of data\n");
    //0 bytes were sent
    return 0;
  }

  LOG_INFO("Sent " + std::to_string(result) + " bytes of data\n");

  //return how many were sent
  return result;
}

int TcpServer::s_accept()
{
  socklen_t size = 0;
  struct sockaddr_in addr;
  int new_fd = accept(fd, (struct sockaddr*)&addr, &size);

  if (new_fd > 0)
  {

    std::cout << "Connected s_accept()\n";
    c_fd = new_fd;

    const int       optVal = 1;
    const socklen_t optLen = sizeof(optVal);
    if (setsockopt(c_fd, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen) != 0)
    {
      LOG_INFO("setsockopt() failed, sockets may appear binded for a while after closing the program\n");
    }

    connected = true;

    return 1;
  }

  else
  {
    LOG_ERROR("accept() failed\n");
    return -1;
  }
}

void TcpServer::s_close()
{
  close(c_fd);
  close(fd);
}

void TcpServer::send_sig(PROTOCOL_ERR sig)
{
  uint16_t byte = htons(sig);
  send(fd,&byte,2,0);
}

int TcpServer::m_fd()
{
  return fd;
}

int TcpServer::m_cfd()
{
  return c_fd;
}

bool TcpServer::m_connected()
{
  return connected;
}

bool TcpServer::m_failure()
{
  return failure;
}

size_t TcpServer::available()
{
  int count;
  ioctl(fd, FIONREAD, &count);

  return (size_t)count;
}

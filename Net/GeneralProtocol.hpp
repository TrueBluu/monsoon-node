#pragma once
#include <cstdint>
#include <vector>
#include <string>
#include <cstring>

enum PROTOCOL_OPERATION : unsigned short
{
  PROTOCOL_ADD_FILE_FRAGMENT = 1 << 0,
  PROTOCOL_RETRIEVE_FILE_FRAGMENT = 1 << 1,
  PROTOCOL_DELETE_FILE_FRAGMENT = 1 << 2,
  PROTOCOL_EDIT_FILE_FRAGMENT = 1 << 3,

};

enum PROTOCOL_ERR : unsigned short
{
  PROTOCOL_SUCCESS = 1 << 0,
  PROTOCOL_FAILURE = 1 << 1,
  PROTOCOL_FILE_DOESNT_EXIST = 1 << 2,
  //Hosts may return PROTOCOL_REJECTED if it exceeds their max file fragment size limit
  PROTOCOL_REJECTED = 1 << 3,
  PROTOCOL_ACK = 1 << 4,
  PROTOCOL_AGAIN = 1 << 5,
  PROTOCOL_PACKET_MALFORMED = 1 << 6,
};

/*
              PUTTING REQUESTS TO THE NETWORK

PROTOCOL_ADD_FILE_FRAGMENT:
  (HOST AUTH)
  (Packet 1 : Total Size of Packet 2 (uint32_t) ) (Packet 2 : [0-1 PROTOCOL_ADD_FILE_FRAGMENT] [2-33 256 bit Elliptic Curve Public Key] [FILE NAME SIZE (FNAMESIZE) 34-37] [FILE NAME 38 - 38+FNAMESIZE] [4 BYTES DATA SIZE (In network order)]) (PACKET 3 : Data)
  (HOST RESPONSE)
  (Packet 1 : Total Size of Packet 2 (uint32_t)) (Packet 2 : [0-1 PROTOCOL_FILE_DOESNT_EXIST / PROTOCOL_FAILURE / PROTOCOL_REJECTED])
  OR
  (Packet 1 : Total Size of Packet 2 (uint32_t)) (Packet 2 : [0-1 PROTOCOL_SUCCESS])
PROTOCOL_RETRIEVE_FILE_FRAGMENT:
  (HOST AUTH)
  (Packet 1 : Total Size of Packet 2 (uint32_t) ) (Packet 2 : [0-1 PROTOCOL_PROTOCOL_RETRIEVE_FILE_FRAGMENT] [2-34 256 bit Elliptic Curve Public Key] [35-39 FILE NAME SIZE (In network order)] [40-... FILE NAME])
  (HOST RESPONSE)
  (Packet 1 : Total Size of Packet 2 (uint32_t) ) (Packet 2 : [0-1 PROTOCOL_FILE_DOESNT_EXIST / PROTOCOL_FAILURE / PROTOCOL_REJECTED])
  OR
  (Packet 1 : Total Size of Packet 2 (uint32_t) ) (Packet 2 : [0-1 PROTOCOL_SUCCESS] [2-6 DATA SIZE (In network order)] [7-... DATA])
*/



struct AddFilePacket
{
  AddFilePacket(uint32_t _size = 0, const unsigned char* pubkey = nullptr,
    std::string name = "", std::vector<unsigned char> _data = {}) :
      size(_size),
      fname(name),
      data(_data)
      {
        if (pubkey != nullptr)
          //Copy over
          memcpy(ECC_PUB, pubkey, 32);
      };

  inline void clean()
  {
    delete[] ECC_PUB;
  };

  int parse();
  int g_raw();
//There's really no point in this being here but it separates the variables from the functions
public:
  uint32_t size;
  uint32_t datasize;
  const PROTOCOL_OPERATION op = PROTOCOL_ADD_FILE_FRAGMENT;
  unsigned char* ECC_PUB = new unsigned char[32];
  std::string fname;
  std::vector<unsigned char> data;

  //TODO pass these as function parameters instead of member variables
  //prefix is the size packet, raw is the packet containing the operation, rsa key, etc..., data is well, data!
  //it's called io_data because it's the input for parse and output for g_raw
  std::vector<unsigned char> prefix, raw, io_data;

};

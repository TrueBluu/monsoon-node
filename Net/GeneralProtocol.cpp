#include <string.h>
#include <arpa/inet.h>
#include <Util/Util.hpp>
#include "GeneralProtocol.hpp"

//TODO Add more checks here
int AddFilePacket::parse()
{
  if (prefix.size() < 4)
    return -1;

  size = ntohl(*(uint32_t*)prefix.data());

  //copy rsa key
  memcpy(ECC_PUB, raw.data() + 2, 32);
  uint32_t fnamesize = ntohl(*(uint32_t*)(raw.data() + 34));
  fname = std::string(reinterpret_cast<char const*>(raw.data() + 38), fnamesize);
  data = std::vector<unsigned char>(io_data);
  datasize = ntohl(*(uint32_t*)(raw.data() + 38 + fnamesize));

  return 0;
}

void pushback_bytes(std::vector<unsigned char>& v, const unsigned char* bytes, uint32_t size)
{
  for (int i = 0; i < size; i++)
  {
    v.push_back(bytes[i]);
  }
}

//TODO add some checks here
//Like checking if ECC_PUB is nullptr
int AddFilePacket::g_raw()
{
  //Prefix contains size of header packet as a uint32_t
  prefix.resize(4);
  //clear vector in case there was data in it already
  raw.clear();


  //Add opcode to packet
  //htonl because all integers are sent with network order endianness (big endian)
  uint16_t op_bytes = htonl(op);
  pushback_bytes(raw, (const unsigned char*)&op_bytes, 2);
  //

  //Add rsa key to packet
  pushback_bytes(raw, ECC_PUB, 32);

  //File name length
  uint32_t fnamesize = htonl(fname.size());
  pushback_bytes(raw, (const unsigned char*)&fnamesize, 4);

  //File name
  pushback_bytes(raw, reinterpret_cast<const unsigned char*>(fname.c_str()), fname.size());

  uint32_t data_size = htonl(data.size());
  pushback_bytes(raw, (const unsigned char*)&data_size, 4);

  //copy data to output vector
  io_data = std::vector<unsigned char>(data);

  size = htonl(raw.size());

  pushback_bytes(prefix, (unsigned char*)&size, 4);

  return 0;
}

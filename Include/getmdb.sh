curl -OL https://github.com/mongodb/mongo-cxx-driver/releases/download/r3.6.3/mongo-cxx-driver-r3.6.3.tar.gz
tar -xzf mongo-cxx-driver-r3.6.3.tar.gz
cd mongo-cxx-driver-r3.6.3/build


cmake ..                                            \
    -DCMAKE_BUILD_TYPE=Release                      \
    -DCMAKE_PREFIX_PATH=/opt/mongo-c-driver         \
    -DCMAKE_INSTALL_PREFIX=/opt/mongo-cxx-driver

sudo cmake --build .. --target EP_mnmlstc_core

cmake --build .
sudo cmake --build . --target install

rm -rf mongo-cxx-driver-r3.6.3
rm -rf mongo-cxx-driver-r3.6.3.tar.gz

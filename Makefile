CLIENT_SRC = $(wildcard Client/*.cpp) $(wildcard Net/*.cpp) $(wildcard Net/Socket/*.cpp) $(wildcard Util/*.cpp)
HOST_SRC = $(wildcard Host/*.cpp) $(wildcard Net/*.cpp) $(wildcard Net/Socket/*.cpp) $(wildcard Util/*.cpp)
CXXFLAGS = -I . -I Include -lpthread -ggdb3


client:
	g++ $(CLIENT_SRC) $(CXXFLAGS) -o fileclient.elf
host:
	g++ $(HOST_SRC) $(CXXFLAGS) -I /opt/mongo-cxx-driver/include -L/opt/mongo-cxx-driver/lib64 -o filehost.elf

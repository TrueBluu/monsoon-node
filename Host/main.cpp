#include <iostream>
#include <Net/Socket/TcpServer.hpp>
#include <Util/Util.hpp>
#include <sys/select.h>
#include <Net/GeneralProtocol.hpp>
#include <map>
#include <cstring>
#include <thread>
#include "Protocol.hpp"

#define MAX_CONN 5

int main()
{
  std::map<int,std::unique_ptr<TcpServer>> servermap;
  HostProtocol proto(MAX_CONN);

  fd_set listening;

  FD_ZERO(&listening);

  for (int i = 0; i < MAX_CONN; i++)
  {
    TcpServer serv = proto.servers[i];
    servermap[serv.m_fd()] = std::unique_ptr<TcpServer>(proto.servers.data() + i);

    FD_SET(serv.m_fd(), &listening);
  }

  //select is destructive, so this is an intermediate
  fd_set ready;

  while(1)
  {
    ready = listening;
    if (select(FD_SETSIZE, &ready, NULL, NULL, NULL) > 0)
    {
      for (int i = 0; i < FD_SETSIZE; i++)
      {
        if (FD_ISSET(i, &ready))
        {
          TcpServer serv = *(servermap[i].get());
          LOG_INFO("Client connecting\n");
          serv.s_accept();

          if (serv.m_connected())
          {
            std::cout << "Connected!\n";
            FD_CLR(serv.m_fd(), &listening);
            proto.spawn_worker(serv);
          }
        }
      }
    }
  }

  return 0;
}

#pragma once
#include <Net/GeneralProtocol.hpp>
#include <Net/Socket/TcpServer.hpp>
#include <Net/Socket/TcpConnection.hpp>
#include <thread>
#include <exception>

#define BASE_PORT 40000

struct HostProtocol
{
  HostProtocol(int maxconn) : m_maxconn(maxconn)
  {
    for (int i = 0; i < maxconn; i++)
    {
      servers.push_back(TcpServer(BASE_PORT + i));
    }
  };

  void stop()
  {
    for (int i = 0; i < servers.size(); i++)
    {
      if (servers[i].m_connected())
        servers[i].s_close();
    }
  }

  int spawn_worker(TcpServer server);
  static int parse(AddFilePacket);

  std::vector<TcpServer> servers;

private:
  std::vector<std::thread> workers;
  int m_maxconn;
};

#include <stdio.h>
#include <Util/Util.hpp>
#include <iostream>
#include <arpa/inet.h>
#include "Protocol.hpp"

//Here's a towel
//DRY yourself
//jokes aside, in worker(), there was alot of code like this, to make it neater, just compressed it into one function
//try_limit is the amount of times to tolerate failure,
//amount is the amount of data to receive
std::vector<unsigned char> worker_loop(TcpServer serv, int try_limit, int amount)
{
  std::vector<unsigned char> buffer;

  for (int i = 0; i < try_limit; i++)
  {
    buffer = serv.s_read(amount);

    if (buffer.size() < amount)
    {
      serv.send_sig(PROTOCOL_AGAIN);
      continue;
    }

    else
    {
      serv.send_sig(PROTOCOL_ACK);
      return buffer;
    }
  }

  serv.send_sig(PROTOCOL_FAILURE);
  return {};
}

void worker(TcpServer serv, int id,std::vector<std::thread>& workers)
{
  //Every packet is prefixed with how big it's going to be, the prefix is how big the header packet will be,
  //the header packet contains the rest of the data,
  std::vector<unsigned char> prefix_buf = worker_loop(serv, 4, 4);
  uint32_t psize = ntohl(*(uint32_t*)(prefix_buf.data()));

  std::vector<unsigned char> pack_header = worker_loop(serv, 4, psize);

  //Get the operation, it is sent in network byte order, so call ntohs
  PROTOCOL_OPERATION op = (PROTOCOL_OPERATION)ntohs(*(uint16_t*)pack_header.data());

  LOG_INFO("HERE\n");

  //This opcode takes an extra packet, which is the data
  if (op == PROTOCOL_ADD_FILE_FRAGMENT)
  {
    //The constructor has many arguments, but most of them have default values
    AddFilePacket pack(psize);

    //Set the inputs
    pack.prefix = prefix_buf;
    pack.raw = pack_header;
    pack.data = {};
    //This is a bit strange, but  we pass the inputs via variables in the struct


    //We need to parse so that we can get the size of the data that we need to receive
    //If parsing failed, we probably got a malformed packet
    if (pack.parse() == -1)
    {
      serv.send_sig(PROTOCOL_PACKET_MALFORMED);
      return;
    }

    std::vector<unsigned char> data = worker_loop(serv, 4, pack.datasize);
  }

  LOG_INFO("Worker Finished!, Detatching\n");
  workers[id].detach();
};

int HostProtocol::spawn_worker(TcpServer server)
{
  std::thread t(worker, server, (int)workers.size(),std::ref(workers));

  //why we use move here: https://stackoverflow.com/questions/27473809/error-use-of-deleted-function-stdthreadthreadconst-stdthread
  workers.push_back(move(t));
  std::cout << "spawned worker\n";
  return 1;
}

int HostProtocol::parse(AddFilePacket packet)
{
  if (!IsSafeStr(packet.fname))
  {
    LOG_INFO("Received a suspicious file path\n");
    return PROTOCOL_REJECTED;
  }

  std::string fullpath = "hoststorage/" + packet.fname;
  FILE* f = fopen(fullpath.c_str(), "wb+");

  if (!f)
  {
    LOG_INFO("fopen failed\n")
    return PROTOCOL_FAILURE;
  }

  fwrite(packet.data.data(),packet.data.size(),1,f);
  fclose(f);
}
